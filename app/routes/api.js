var express = require("express");
var router = express.Router();


router.get("/", function(req,res) {
    res.status(500).send({
        success: false,
        message: "Maaf data tidak tersedia"
    })
});

module.exports = router;