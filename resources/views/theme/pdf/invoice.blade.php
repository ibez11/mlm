@extends('theme.pdf.layout')

@section('content')
<h3 class="text-center">INVOICE</h3>
<div class="ref-no text-center">#INVXXX001</div>

<table class="info" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="text-top">
                <p>Kepada yth,</p>
                <p><strong>Customer Name</strong></p>
                <p>Customer Name</p>
                <p>Customer Name</p>
            </td>
            <td class="text-top">
                <p>Kepada yth,</p>
                <p><strong>Customer Name</strong></p>
                <p>Customer Name</p>
                <p>Customer Name</p>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing="0" class="table-detail" width="100%">
    <thead>
        <tr>
            <td class="col-id text-center">No.</td>
            <td class="text-left">Keterangan</td>
            <td class="col-price text-right">Jumlah</td>
        </tr>
    </thead>
    <tbody>
        <!--Detail-->
        <?php for ($i=1; $i <= 5; $i++): ?>
        <tr>
            <td class="text-center"><?php echo $i; ?></td>
            <td class="text-left">$detail->keterangan</td>
            <td class="text-right">Rp $detail->total,-</td>
        </tr>
        <?php endfor; ?>
    </tbody>
    <tfoot>
        <tr>
            <td class="text-right" colspan="2"><strong>TOTAL<strong></td>
            <td class="text-right"><strong>Rp 10.000.000.000,-</strong></td>
        </tr>
        <tr>
            <td class="text-right" colspan="2">TOTAL</td>
            <td class="text-right">Rp 10.000.000.000,-</td>
        </tr>
        <tr>
            <td class="text-right" colspan="2">TOTAL</td>
            <td class="text-right">Rp 10.000.000.000,-</td>
        </tr>
    </tfoot>
</table>

<table class="table-footer">
    <tr>
        <td></td>
        <td class="text-top text-center">Surabaya, 01 Januari 2019</td>
    </tr>
    <tr>
        <td></td>
        <td class="text-bottom text-center col-sign"><strong>Finance<strong></td>
    </tr>
</table>
@endsection
