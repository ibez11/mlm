(function() {

'use strict';

angular.module('app').config(config);

function config($stateProvider, $urlRouterProvider, $httpProvider) {
    console.log('path', path);

    var states = [];
    var state = null;

    state = {
        name: 'dashboard',
        url: '/dashboard',
        templateUrl: path.dashboard,
        controller: 'DashboardController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'quotation',
        url: '/quotation/list?keyword&status&order_by&page',
        templateUrl: path.penawaran.index,
        controller: 'QuotationController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'quotation-pending',
        url: '/quotation/list/{status_page}?order_by&page',
        templateUrl: path.penawaran.index,
        controller: 'QuotationController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'schedule',
        url: '/schedule/list?keyword&order_by&page&status',
        templateUrl: path.schedule.index,
        controller: 'QuotationScheduleController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'schedule-pending',
        url: '/schedule/list/pending?keyword&order_by&page',
        templateUrl: path.schedule.index,
        controller: 'QuotationScheduleController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: 'pending' },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'quotation-create',
        url: '/quotation/create?parent_id',
        templateUrl: path.penawaran.create,
        controller: 'QuotationController',
        controllerAs: 'vm',
        params: {
           parent_id: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'quotation-detail',
        url: '/quotation/detail/{id}',
        templateUrl: path.penawaran.create,
        controller: 'QuotationController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'quotation-schedule',
        url: '/quotation/schedule/{id}',
        templateUrl: path.penawaran.schedule,
        controller: 'QuotationScheduleController',
        controllerAs: 'vm',
        params: {
           project_id: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'project',
        url: '/project/list?order_by&page',
        templateUrl: path.project.index,
        controller: 'ProjectController',
        controllerAs: 'vm',
        params: {
           rangeStart: { squash: true, value: null },
           rangeFinish: { squash: true, value: null },
           keyword: { squash: true, value: null },
           order_by: { squash: true, value: null },
           filter_by: { squash: true, value: null },
           filter_order_by: { squash: true, value: null },
           page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'project-create',
        url: '/project/create',
        templateUrl: path.project.create,
        controller: 'ProjectController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'project-detail',
        url: '/project/detail/{id}',
        templateUrl: path.project.create,
        controller: 'ProjectController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'project-role',
        url: '/project/{id}/change-worker',
        templateUrl: path.project.worker,
        controller: 'ProjectWorkerController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'project-set-hpp-1',
        url: '/project/{id}/set-hpp-1',
        templateUrl: path.project.setHpp1,
        controller: 'ProjectHpp1Controller',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'project-price-plan',
        url: '/project/{project_id}/price-plan/create',
        templateUrl: path.project.pricePlanCreate,
        controller: 'ProjectPricePlanController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'price-plan',
        url: '/price-plan/list?keyword&order_by&page&status',
        templateUrl: path.pricePlan.index,
        controller: 'ProjectPricePlanController',
        controllerAs: 'vm',
        params: {
            project_id: { squash: true, value: null },
            status: { squash: true, value: null },
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'price-plan-pending',
        url: '/price-plan/list/pending?keyword&order_by&page',
        templateUrl: path.pricePlan.index,
        controller: 'ProjectPricePlanController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status_page: { squash: true, value: 'pending' },
            project_id: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'price-plan-detail',
        url: '/price-plan/detail/{id}',
        templateUrl: path.project.pricePlanCreate,
        controller: 'ProjectPricePlanController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'price-summary-detail',
        url: '/project/{id}/price-summary',
        templateUrl: path.project.priceSummary,
        controller: 'ProjectPriceSummaryController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'price-plan-change-jobs',
        url: '/price-plan/detail/{quotation_id}/change-jobs/{quotation_detail_id}',
        templateUrl: path.project.pricePlanJobs,
        controller: 'ProjectPricePlanJobsController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'project-stock',
        url: '/project/{id}/stock',
        templateUrl: path.project.stock,
        controller: 'ProjectStockController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'customer',
        url: '/customer/list?keyword&order_by&page',
        templateUrl: path.customer.index,
        controller: 'CustomerController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'customer-create',
        url: '/customer/create',
        templateUrl: path.customer.create,
        controller: 'CustomerController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'customer-detail',
        url: '/customer/detail/{id}',
        templateUrl: path.customer.create
    };
    states.push(state);

    state = {
        name: 'item',
        url: '/item/list/{role}?keyword&order_by&page',
        templateUrl: path.item.index,
        controller: 'ItemController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'item-create',
        url: '/item/create/{role}',
        templateUrl: path.item.create
    };
    states.push(state);

    state = {
        name: 'item-detail',
        url: '/item/detail/{role}/{id}',
        templateUrl: path.item.create
    };
    states.push(state);

    state = {
        name: 'role',
        url: '/role/list?keyword&order_by&page',
        templateUrl: path.role.index,
        controller: 'RoleController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'role-set-permission',
        url: '/role/set-permission/{role_id}',
        templateUrl: path.role.setPermission,
        controller: 'RoleController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'role-create',
        url: '/role/create',
        templateUrl: path.role.create,
        controller: 'RoleController',
        controllerAs: 'vm',
    };
    states.push(state);

    state = {
        name: 'role-detail',
        url: '/role/detail/{id}',
        templateUrl: path.role.create,
        controller: 'RoleController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'admin',
        url: '/admin/list?keyword&order_by&page',
        templateUrl: path.admin.index,
        controller: 'AdminController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'admin-role',
        url: '/admin/list/{role}?keyword&order_by&page',
        templateUrl: path.admin.index,
        controller: 'AdminController',
        controllerAs: 'vm',
        params: {
           keyword: { squash: true, value: null },
           order_by: { squash: true, value: null },
           page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'admin-create',
        url: '/admin/create',
        templateUrl: path.admin.create,
        controller: 'AdminController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'admin-create-role',
        url: '/admin/create/{role}',
        templateUrl: path.admin.create,
        controller: 'AdminController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'admin-detail',
        url: '/admin/detail/{id}',
        templateUrl: path.admin.create
    };
    states.push(state);

    state = {
        name: 'profile',
        url: '/profile',
        templateUrl: path.admin.create,
        params: {
           state: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'sp',
        url: '/sp/list?keyword&order_by&page&project_id',
        templateUrl: path.sp.index,
        controller: 'SuratPermintaanController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: "draft,decline" },
            project_id: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'sp-pending',
        url: '/sp/list/pending?keyword&order_by&page',
        templateUrl: path.sp.index,
        controller: 'SuratPermintaanController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: "pending" },
            project: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'sp-create',
        url: '/sp/create?project_id',
        templateUrl: path.sp.create,
        controller: 'SuratPermintaanController',
        controllerAs: 'vm',
        params: {
            project_id: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'sp-detail',
        url: '/sp/detail/{id}',
        templateUrl: path.sp.create,
        controller: 'SuratPermintaanController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'po',
        url: '/purchase-order/list?keyword&order_by&page&project_id&status',
        templateUrl: path.po.index,
        controller: 'PurchaseOrderController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: "approve" },
            project_id: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'po-create',
        url: '/purchase-order/create',
        templateUrl: path.po.create,
        controller: 'PurchaseOrderController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'po-detail',
        url: '/purchase-order/detail/{id}',
        templateUrl: path.po.create,
        controller: 'PurchaseOrderController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'receipt',
        url: '/receipt/list?keyword&order_by&page&project_id&purchase_order_id',
        templateUrl: path.receipt.index,
        controller: 'ReceiptController',
        controllerAs: 'vm',
        params: {
            status: { squash: true, value: "draft,decline,approve" },
            project_id: { squash: true, value: null },
            purchase_order_id: { squash: true, value: null },
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'receipt-pending',
        url: '/receipt/list?keyword&project_id&order_by&page',
        templateUrl: path.receipt.index,
        controller: 'ReceiptController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: 'pending' },
            project_id: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'receipt-detail',
        url: '/receipt/detail/{id}',
        templateUrl: path.receipt.create,
        controller: 'ReceiptController',
        controllerAs: 'vm'
    };
    states.push(state);
    
    state = {
        name: 'receipt-create',
        url: '/receipt/create?purchase_order_id',
        templateUrl: path.receipt.create,
        controller: 'ReceiptController',
        controllerAs: 'vm',
        params: {
            purchase_order_id: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'invoice',
        url: '/invoice/list?keyword&order_by&page&project_id',
        templateUrl: path.invoice.index,
        controller: 'InvoiceController',
        controllerAs: 'vm',
        params: {
            status: { squash: true, value: "draft,decline,approve" },
            keyword: { squash: true, value: null },
            project: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null },
            project_id: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'expense',
        url: '/expense/list?keyword&order_by&page&project_id',
        templateUrl: path.expense.index,
        controller: 'ExpenseController',
        controllerAs: 'vm',
        params: {
            status: { squash: true, value: null },
            project_id: { squash: true, value: null },
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'expense-pending',
        url: '/expense/list?keyword&order_by&page&project_id',
        templateUrl: path.expense.index,
        controller: 'ExpenseController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: 'pending' },
            project_id: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'expense-create',
        url: '/expense/create',
        templateUrl: path.expense.create,
        controller: 'ExpenseController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'expense-detail',
        url: '/expense/detail/{id}',
        templateUrl: path.expense.create,
        controller: 'ExpenseController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'invoice-pending',
        url: '/invoice/list?keyword&order_by&page',
        templateUrl: path.invoice.index,
        controller: 'InvoiceController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: 'pending' },
            project: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'invoice-detail',
        url: '/invoice/detail/{id}',
        templateUrl: path.invoice.create,
        controller: 'InvoiceController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'invoice-create',
        url: '/invoice/create?project_id',
        templateUrl: path.invoice.create,
        controller: 'InvoiceController',
        controllerAs: 'vm',
        params: {
            project_id: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'transfer',
        url: '/transfer/list?keyword&order_by&page&project_id_from&project_id_to&date_from&date_to',
        templateUrl: path.transfer.index,
        controller: 'TransferController',
        controllerAs: 'vm',
        params: {
            project_id_from: { squash: true, value: null },
            project_id_to: { squash: true, value: null },
            date_from: { squash: true, value: null },
            date_to: { squash: true, value: null },
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'transfer-pending',
        url: '/transfer/list/pending?keyword&order_by&page&project_id_from&project_id_to&date_from&date_to',
        templateUrl: path.transfer.index,
        controller: 'TransferController',
        controllerAs: 'vm',
        params: {
            project_id_from: { squash: true, value: null },
            project_id_to: { squash: true, value: null },
            date_from: { squash: true, value: null },
            date_to: { squash: true, value: null },
            keyword: { squash: true, value: null },
            status_page: { squash: true, value: 'pending' },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'transfer-detail',
        url: '/transfer/detail/{id}',
        templateUrl: path.transfer.create,
        controller: 'TransferController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'transfer-create',
        url: '/transfer/create',
        templateUrl: path.transfer.create,
        controller: 'TransferController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'mutasi-stok',
        url: '/mutasi-stok/list?keyword&date_from&date_to&item&type&order_by&page',
        templateUrl: path.mutasi.index,
        controller: 'MutasiStokController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            date_from: { squash: true, value: null },
            date_to: { squash: true, value: null },
            item: { squash: true, value: null },
            type: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'stock-flow',
        url: '/stock-flow/list?project_id&item_id&category&date_from&date_to&order_by&page',
        templateUrl: path.stockFlow.index,
        controller: 'StockFlowController',
        controllerAs: 'vm',
        params: {
            project_id: { squash: true, value: null },
            item_id: { squash: true, value: null },
            category: { squash: true, value: null },
            date_from: { squash: true, value: null },
            date_to: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'jobs',
        url: '/jobs/list?keyword&order_by&page&status',
        templateUrl: path.jobs.index,
        controller: 'JobsController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'jobs-create',
        url: '/jobs/create',
        templateUrl: path.jobs.create,
        controller: 'JobsController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'jobs-detail',
        url: '/jobs/detail/{id}',
        templateUrl: path.jobs.create,
        controller: 'JobsController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'category',
        url: '/unit/list?order_by',
        templateUrl: path.category.index,
        controller: 'UnitController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'stock-opname',
        url: '/stock-opname/list?order_by&status',
        templateUrl: path.stockOpname.list,
        controller: 'StockOpnameController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'stock-opname-detail',
        url: '/stock-opname/detail/{id}',
        templateUrl: path.stockOpname.create,
        controller: 'StockOpnameController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'stock-opname-create',
        url: '/stock-opname/create',
        templateUrl: path.stockOpname.create,
        controller: 'StockOpnameController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'bank',
        url: '/bank/list?order_by',
        templateUrl: path.bank.index,
        controller: 'BankController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'job-opname',
        url: '/job-opname/list?keyword&order_by&page&status',
        templateUrl: path.jobOpname.index,
        controller: 'JobOpnameController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'job-opname-create',
        url: '/job-opname/create',
        templateUrl: path.jobOpname.create,
        controller: 'JobOpnameController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'job-opname-detail',
        url: '/job-opname/detail/{id}',
        templateUrl: path.jobOpname.create,
        controller: 'JobOpnameController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'checklist-detail',
        url: '/project/{project_id}/checklist/detail',
        templateUrl: path.checklist.create,
        controller: 'ChecklistController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'checklist-tick-create',
        url: '/project/{project_id}/checklist/tick/create?user_id',
        templateUrl: path.checklistTick.create,
        controller: 'ChecklistTickController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'invoice-payment',
        url: '/invoice/mandor-vendor/list?keyword&order_by&page',
        templateUrl: path.invoicePayment.index,
        controller: 'InvoicePaymentController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'invoice-payment-pending',
        url: '/invoice/mandor-vendor/list/pending?keyword&order_by&page',
        templateUrl: path.invoicePayment.index,
        controller: 'InvoicePaymentController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            status: { squash: true, value: "pending" },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'invoice-payment-create',
        url: '/invoice/job-opname/create?opname_id',
        templateUrl: path.invoicePayment.create,
        controller: 'InvoicePaymentController',
        controllerAs: 'vm',
        params: {
            opname_id: { squash: true, value: null }
        }
    };
    states.push(state);

    state = {
        name: 'invoice-payment-detail',
        url: '/invoice-payment/detail/{id}',
        templateUrl: path.invoicePayment.create,
        controller: 'InvoicePaymentController',
        controllerAs: 'vm'
    };
    states.push(state);

    state = {
        name: 'setting',
        url: '/setting',
        templateUrl: path.setting.index,
        controller: 'SettingController',
        controllerAs: 'vm',
        params: {
            keyword: { squash: true, value: null },
            order_by: { squash: true, value: null },
            page: { squash: true, value: null }
        }
    };
    states.push(state);

    // For testing only
    state = {
        name: 'demo',
        url: '/demo',
        templateUrl: path.demo.index,
        controller: 'DemoController',
        controllerAs: 'vm'
    };
    states.push(state);

    for(var i=0; i<states.length; i++)
        $stateProvider.state(states[i]);

    $urlRouterProvider.otherwise('quotation/list');
    // alternatively, register the interceptor via an anonymous factory
    $httpProvider.interceptors.push(function($q) {
      return {
        'response': function(resp) {
            var data = resp.data;

            if(data.status === 401) {
                // Force logout
                window.location = '/logout';
            }

            return resp;
        }
      };
    });
}

})();
