(function() {
'use strict';

angular.module('app').controller('PurchaseOrderController', PurchaseOrderController);

function PurchaseOrderController($state, $window, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.reverse = true;
    this.propertyName = 'name';
    this.sort = {
        active: '',
        descending: undefined
    } 

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;
    this.po = true;

    // Quotation List
    this.list = null;

    this.setStatus = null;
    this.tabTitle = 'Purchase Order';

    this.createEmptyData = function() {
        this.data = {
            id: null,
            project_id: null,
            status_id: null,
            status: {
                name: 'draft'
            },
            is_open: null,
            ref_no: null,
            notes: null,
            is_tax: 0,
            created: null,
            created_at: null,
            updated_at: null,
            detail: [],

            // List of detail id that want to be deleted
            _delete_detail: []
        };
    };

    this.isCreate = function() {
        return !$stateParams.id;
    };

    this.isDraft = function() {
        return this.isStatus('draft');
    };

    this.isApproved = function() {
        return this.isStatus('approve');
    };

    this.isDecline = function() {
        return this.isStatus('decline');
    };

    this.isPending = function() {
        return this.isStatus('pending');
    };

    this.isStatus = function(status) {
        return this.data.status.name === status;
    };

    this.init = function() {
        console.log('Purchase Order list init');
        var search = vm.search;

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.project_id)
            search.project_id = $stateParams.project_id;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.status_page == 'pending')
            this.listPending = true;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        if (this.listPending == true) {
            vm.search.status = 'pending';
        }
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.PurchaseOrder.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
        this.getProjectList();
    };
    
    this.getProjectList = function() {
        ApiService.Project.all({page: 'all'}).then(function(resp) {
            var data = resp.data;
            vm.projects = data.data;
        });
    };

    this.create = function() {
        vm.ls.get('loading').on();
        console.log('create');
        this.createEmptyData();
        // For detail quotation
        var id = $stateParams.id;

        if(id) {
            vm.ls.get('loading').on();
            ApiService.PurchaseOrder.get(id).then(function(resp) {
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    data.project_id = String(data.project_id);

                    // Init for delete detail
                    data._delete_detail = [];
                    vm.loadPaymentList(data.id);
                    vm.data = data;
                    vm.data.total = parseFloat(vm.data.total);
                    for(var i=0; i<vm.data.detail.length; i++) {
                        vm.data.detail[i]._hash = vm._getRandomHash();
                    }
                    vm.ppn = 0;
                    if(vm.data.is_tax == 1)
                        vm.ppn = (parseFloat(vm.data.total) * 10) / 100;
                    console.log('ajax detail', vm.data);
                } else {
                    vm.ls.get('loading').on();
                    vm.message = resp.data.errors;
                }
                vm.ls.get('loading').off();
            });

            this.loadReceipt(id);
        }
        this.loadBankList();
        console.log(vm.data);
    };

    this.newPayment = function() {
        var paidAmount = Math.round((parseFloat(vm.data.total) + parseFloat(vm.ppn)) - vm.totalPayment);
        this.payment = {
            id: null,
            purchase_order_id: null,
            bank_category_id: vm.bank.id,
            amount: paidAmount,
            account_number: null,
            notes: null,
            created: DateFormatter.date(new Date()),
        };
    };

    this.editPayment = function(id) {
            ApiService.PurchaseOrder.Payment.get(id).then(function(resp) {
                console.log(resp);
                var data = resp.data;
                vm.payment = data.data;
                
                vm.loadBankList();

                vm.payment.bank_category_id = String(vm.payment.bank_category_id);
            });
        };

    this.loadReceipt = function(purchaseOrderId) {
        // Load Receipt list
        ApiService.Receipt.all({page: 'all', purchase_order_id: purchaseOrderId})
        .then(function(resp) {
            var data = resp.data;
            console.log('receipt',data);
            vm.receipt = data.data;
        });
    }

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    this.submitNotes = function() {
        vm.ls.get('loading').on();
        
        var postData = this._createPostDataNotes(this.data);
        console.log('posData',postData);
        ApiService.PurchaseOrder.create(postData).then(function(resp){
            if(!resp.data.is_error) {
                vm.create();
                
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
            vm.ls.get('loading').off();
        });
    };

    this._createPostDataNotes = function(data)
    {
        var postData = JSON.parse(JSON.stringify(data));
        postData.change_notes = true;

        delete postData.revisions;
        delete postData.detail;
        delete postData.project;

        return postData;
    }

    this.submitPayments = function() {
        vm.ls.get('loading').on();
        console.log('post data', this.payment);
        var postData = this._createPayment(this.payment);
        
        ApiService.PurchaseOrder.Payment.add(postData).then(function(resp){
            if(!resp.data.is_error) {
                vm.loadPaymentList(vm.data.id);
                vm.newPayment();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
            vm.ls.get('loading').off();
        });
    };

    this._createPayment = function(data){
        data.purchase_order_id = vm.data.id;
        var postData = JSON.parse(JSON.stringify(data));

        return postData;
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('po', this.search);    
    
    };
    //load payment
    this.loadPaymentList = function(purchaseOrderId) {
        var search = {
            purchase_order_id: purchaseOrderId,
            page: 'all'
        }
        
        ApiService.PurchaseOrder.Payment.all(search).then(function(resp) {
            console.log(resp);
            var data = resp.data;
            vm.payments = data.data;
            vm.totalPayment = 0;
            angular.forEach(vm.payments, function (value, key) {
                vm.totalPayment += parseFloat(value.amount);
            });
            
        });
    };

    this.deletePayment = function(id) {
        vm.ls.get('loading').on();
        ApiService.PurchaseOrder.Payment.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        console.log(vm.sendToManager, this.data);

        var postData = this._createPostData(this.data);

        if(vm.sendToManager == 1) {
            postData.status = 'pending';
        }
        vm.sendToManager = 0; // Set back to 0

        console.log('post data', postData);
        ApiService.Quotation.create(postData).then(function(resp){
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('quotation-detail', { 'id': resp.data.data });
                else
                    vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
            vm.ls.get('loading').off();
        });

    };

    this._getRandomHash = function() {
        var s1, s2;
        s1 = String(Math.floor(Math.random()*1000));
        s2 = String(new Date().getTime());
        return s1 + s2;
    };

    this._createPostData = function(data){
        var postData = JSON.parse(JSON.stringify(data));

        console.log("Data : ", postData);

        delete postData.revisions;

        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i]._hash;
            delete postData.detail[i].$$hashKey;
        }

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.PurchaseOrder.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.loadBankList = function(purchaseOrderId) {
        var search = {
            group_by: 'bank',
            page: 'all'
        }

        ApiService.Bank.all(search).then(function(resp) {
            console.log(resp);
            var data = resp.data;
            vm.banks = data.data;
            vm.bank = vm.banks[0];
            for (var i = 0; i < vm.banks.length; i++) {
                vm.banks[i].id = String(vm.banks[i].id);
            }

        });
    };

    this.deleteReceipt = function(receiptId) {
        var id = receiptId;
        vm.ls.get('loading').on();
        ApiService.Receipt.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.sortByAngular = function(propertyName) {
        var sort = this.sort;

        if (sort.active == propertyName) {
            sort.descending = !sort.descending;
        } 
        else {
            sort.active = propertyName;
            sort.descending = false;
        }

        this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
        this.propertyName = propertyName;
    };

    this.getIconSort = function(column) {
    var sort = vm.sort;
    if (sort.active == column) {
        return sort.descending
        ? 'fa-sort-desc'
        : 'fa-sort-asc';
        }
    return 'fa-sort';
    }

    this.download = function(id){
        ApiService.PurchaseOrder.downloadPo(id).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

    this.setApproved = function(id) {
        this.setStatus(id, 'approve');
    };

    this.setDeclined = function(id) {
        this.setStatus(id, 'decline');
    };

    this.setDraft = function(id) {
        this.setStatus(id, 'draft');
    };

    this.setStatus = function(id, status) {
        vm.ls.get('loading-' + id).on();
        ApiService.PurchaseOrder.setStatus(id, status).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                $state.go('sp-detail', { 'id': id });
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading-' + id).off();
        });
    };

    this.setPpn = function(id, tax){
        vm.ls.get('loading-ppn').on();
        ApiService.PurchaseOrder.setTax(id, tax).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                // $state.go('sp-detail', { 'id': id });
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading-ppn').off();
        });
    }

    this.setTax = function(id) {
        this.setPpn(id, '1');
        vm.ppn = (parseFloat(vm.data.total) * 10 ) / 100;
    };

    this.setNonTax = function(id) {
        this.setPpn(id, '0');
        vm.ppn = 0;
    };

    this.openTab = function (name) {
        if (name == 'po') {
            vm.pembayaran = false;
            vm.lpb = false;
            vm.po = true;
            vm.tabTitle = 'Purchase Order';
        }
        if (name == 'lpb') {
            vm.pembayaran = false;
            vm.lpb = true;
            vm.po = false;
            vm.tabTitle = 'LPB';
        }
        if (name == 'pembayaran') {
            vm.pembayaran = true;
            vm.lpb = false;
            vm.po = false;
            vm.tabTitle = 'Pembayaran';
        }
    };
};
})();