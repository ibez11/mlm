(function() {
'use strict';

angular.module('app').controller('AdminController', AdminController);

function AdminController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.permissions = {
        admin: ['admin_create', 'admin_delete', 'admin_detail',
            'admin_list'],
        supplier: ['supplier_create', 'supplier_delete', 'supplier_detail',
            'supplier_list'],
        mandor: ['mandor_create', 'mandor_delete', 'mandor_detail',
            'mandor_list'],
        vendor: ['vendor_create', 'vendor_delete', 'vendor_detail',
            'vendor_list']
    };

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();

    this.titleRole = null;
    this.role = $stateParams.role;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    this.list = null;
    this.exceptRoles = 'supplier,mandor,vendor';

    this.createEmptyData = function() {
        this.data = {
            id: null,
            role_id: null,
            role: 'direktur',
            city_id: null,
            username: null,
            password: null,
            confirm_password: null,
            email: null,
            name: null,
            address: null,
            phone: null
        };
    };

    this.setTitle = function() {
        var role = this.role;

        if (role == "supplier") {
            this.titleRole = "List Supplier";
        } else if (role == "mandor") {
            this.titleRole = "List Mandor";
        } else if (role == "vendor") {
            this.titleRole = "List Vendor";
        } else {
            this.titleRole = "List Admin";
        }
    };

    this.init = function() {
        this.createEmptyData();
        this.setTitle();
        var search = {};

        // If role is NOT empty then this mean is NOT admin
        if($stateParams.role){
            search.roles = $stateParams.role; // Get only 1 role from $role variable
            this.data.role = $stateParams.role;
        } else {
            // Get all admin
            search.except_roles = this.exceptRoles;
        }
        console.log('role', search);

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    // Get correct permission from string based on role
    this.getPermission = function(permission) {
        var permissionTable = this.permissions.admin;
        switch(this.role) {
            case 'mandor':
                permissionTable = this.permissions.mandor;
                break;
            case 'vendor':
                permissionTable = this.permissions.vendor;
                break;
            case 'supplier':
                permissionTable = this.permissions.supplier;
                break;
        }

        // console.log(permissionTable);
        // debugger;

        var newPermission = null;
        switch(permission) {
            case 'create':
                newPermission = permissionTable[0];
                break;
            case 'delete':
                newPermission = permissionTable[1];
                break;
            case 'detail':
                newPermission = permissionTable[2];
                break;
            case 'list':
                newPermission = permissionTable[3];
                break;
        }

        return newPermission;
    }

    // Search invoice
    this._doSearch = function() {
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.Admin.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });

        // Load role list
        var status = {
            group_by: "role"
            // except_roles: vm.exceptRoles
        };
        ApiService.Category.all(status).then(function(resp) {
            console.log(resp);
            var data = resp.data;
            vm.roles = data.data;
        });
    };

    // Redirect to correct route
    this.doSearch = function() {
        this.search.order_by = this.orderBy.toString();
        $state.go('admin', this.search);
    };

    this.create = function() {
        this.createEmptyData();
        // Update role
        if($stateParams.role){
            this.data.role = $stateParams.role;
        }

        var id = $stateParams.id;
        if(id){
            vm.ls.get('loading').on();
            ApiService.Admin.get(id).then(function(resp) {
                if(!resp.data.is_error) {
                    console.log(resp);
                    var data = resp.data.data;
                    if (data.city_id) {
                        data.city_id = String(data.city_id);
                    }
                    if (data.role_id) {
                        data.role_id = String(data.role_id);
                    }

                    vm.data = data;
                } else {
                    vm.message = resp.data.errors;
                    if ($stateParams.role) {
                        $state.go('admin-role');
                    } else {
                        $state.go('admin');  
                    }
                }
                vm.ls.get('loading').off();
            });
        }

        if ($stateParams.state) {
            vm.ls.get('loading').on();
            ApiService.User.profile().then(function(resp) {
                if(!resp.data.is_error) {
                    console.log(resp);
                    var data = resp.data.data;
                    data.city_id = String(data.city_id);
                    data.role_id = String(data.role_id);

                    vm.data = data;
                } else {
                    vm.message = resp.data.errors;
                    if ($stateParams.role) {
                        $state.go('admin-role');
                    } else {
                        $state.go('admin');  
                    }
                }
                vm.ls.get('loading').off();
            });
        }

        // Load role list
        ApiService.Category.all({'group_by': 'role'}).then(function(resp) {
            var data = resp.data;
            vm.roles = data.data;
        });

        // Load cities list
        ApiService.City.all().then(function(resp) {
            var data = resp.data;
            vm.cities = data.data;
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        if ($stateParams.role) {
            $state.go('admin-role', this.search);
        } else {
            $state.go('admin', this.search);  
        }
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        var postData = this._createPostData(this.data);
        // postData.role = this.role;
        console.log(postData);
        ApiService.Admin.create(postData).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('admin-detail', { 'id': resp.data.data });

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });

    };

    this._createPostData = function(data){
        var role = data.role;
        if ($stateParams.role) {
            role = $stateParams.role;
        }

        var postData = {
            id: data.id,
            role_id: data.role_id,
            role: role,
            city_id: data.city_id,
            username: data.username,
            password: data.password,
            confirm_password: data.confirm_password,
            email: data.email,
            name: data.name,
            address: data.address,
            phone: data.phone
        };

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Admin.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.isSupplier = function() {
        return this.isRole('supplier');
    };

    this.isMandor = function() {
        return this.isRole('mandor');
    };

    this.isVendor = function() {
        return this.isRole('vendor');
    };

    this.isAdmin = function() {
        return !this.isSupplier() && !this.isVendor() && !this.isMandor();
    };

    this.isRole = function(role) {
        return this.data.role === role;
    };
};
})();