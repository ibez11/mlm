(function() {
'use strict';

angular.module('app').controller('ScheduleController', ScheduleController);

function ScheduleController($state, $window, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.isEditJob = false;
    this.listPending = false;
    this.reverse = true;
    this.propertyName = 'name';
    this.sort = {
        active: '',
        descending: undefined
    } 

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Quotation List
    this.list = null;

    // Temp job in box bottom
    this.tempJob = {
        id: 0,
        qty: 0,
        price: 0
    };

    this.setStatus = null;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            quotation_id: null,
            customer_id: null,
            city_id: null,
            ref_no: null,
            name: null,
            address: null,
            created: null,
            created_at: null,
            updated_at: null,
            status: {
                name: 'draft'
            },
            revisions: [],
            detail: [],

            // List of detail id that want to be deleted
            _delete_detail: []
        };
    };

    this.scrollToJob = function() {
        scrollTo($('#job'), 500);
    };

    this.isCreate = function() {
        return !$stateParams.id;
    };

    this.isDraft = function() {
        return this.isStatus('draft') || $stateParams.parent_id;
    };

    this.isPending = function() {
        return this.isStatus('pending');
    };

    this.isRevision = function() {
        return this.isStatus('revision');
    };

    this.isStatus = function(status) {
        var data = this.data;

        return this.data.status.name === status;
    };

    this.init = function() {
        console.log('Quotation list init');
        var search = vm.search;

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.status_page == 'pending')
            this.listPending = true;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        if (this.listPending == true) {
            vm.search.status = 'pending';
        }
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.Quotation.scheduleList(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
        // Load Customer list
        var status = {
            group_by: "status",
            name: "pending"
        };
        ApiService.Category.all(status).then(function(resp) {
            var data = resp.data;
            vm.status = data.data;
        });
    };
    
    this.create = function() {
        this.createEmptyData();
        // For detail quotation
        var id = $stateParams.id;

        console.log('parent id', $stateParams);
        if($stateParams.parent_id) {
            id = $stateParams.parent_id;
        }

        if(id) {
            vm.ls.get('loading').on();
            ApiService.Quotation.get(id).then(function(resp) {
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    data.customer_id = String(data.customer_id);
                    data.city_id = String(data.city_id);

                    if($stateParams.parent_id) {
                        data.parent_id = $stateParams.parent_id;
                    }

                    // Init for delete detail
                    data._delete_detail = [];

                    vm.data = data;
                    for(var i=0; i<vm.data.detail.length; i++) {
                        vm.data.detail[i]._hash = vm._getRandomHash();
                    }
                    vm.groups = vm._groupingDetail(vm.data.detail);
                    console.log('ajax detail', vm.data);
                } else {
                    vm.ls.get('loading').on();
                    vm.message = resp.data.errors;
                }
                vm.ls.get('loading').off();
            });
        }

        // Load Customer list
        ApiService.Customer.all({page: 'all'}).then(function(resp) {
            var data = resp.data;
            vm.customers = data.data;
        });

        // Load cities list
        ApiService.City.all().then(function(resp) {
            var data = resp.data;
            vm.cities = data.data;
        });
        console.log(vm.data);
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        if(this.listPending == true){
            this.search.order_by = this.orderBy.toString();
            this.search.status_page = 'pending';
            $state.go('quotation-pending', this.search);
        } else {
            this.search.order_by = this.orderBy.toString();
            $state.go('quotation', this.search);    
        }
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        console.log(vm.sendToManager, this.data);

        var postData = this._createPostData(this.data);

        if(vm.sendToManager == 1) {
            postData.status = 'pending';
        }
        vm.sendToManager = 0; // Set back to 0

        console.log('post data', postData);
        ApiService.Quotation.create(postData).then(function(resp){
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('quotation-detail', { 'id': resp.data.data });
                else
                    vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
            vm.ls.get('loading').off();
        });

    };

    this.newJob = function() {
        this.tempJob = {
            qty: 0,
            price: 0,
            _hash: this._getRandomHash()
        };
    };

    this.deleteJob = function(job) {
        if(job.id)
            this.data._delete_detail.push(job.id);

        this.data.detail.removeByObject(job);
        this.groups = this._groupingDetail(this.data.detail);
    }

    this.editJob = function(job) {
        this.tempJob = job;
    };

    this.addJob = function() {
        var isNew = true;
        console.log('add job');

        // Check if this job update
        for(var i=0; i<this.data.detail.length; i++) {
            if(this.data.detail[i]._hash === this.tempJob._hash) {
                isNew = false;
            }
        }

        if(isNew)
            // If new just push to detail
            this.data.detail.push(this.tempJob);

        // and rerender it
        this.groups = this._groupingDetail(this.data.detail);
        this.newJob();

        return;
    };

    this._getRandomHash = function() {
        var s1, s2;
        s1 = String(Math.floor(Math.random()*1000));
        s2 = String(new Date().getTime());
        return s1 + s2;
    };

    // Grouping quote details
    this._groupingDetail = function(details) {
        var list = [];
        var i, x;
        var group;
        var total = 0;

        for(i=0; i<details.length; i++) {
            x = details[i];

            group = list.find(function(el) {
                return el.name === x.group_by;
            });

            if(group){
                total += (x.qty * x.price);
                group.detail.push(x);
                group.total = total;
            }else{
                total = (x.qty * x.price);
                list.push({
                    name: x.group_by,
                    total : total,
                    detail: [x]
                });
            }
        }
        return list;
    };

    this._createPostData = function(data){
        var postData = JSON.parse(JSON.stringify(data));

        console.log("Data : " + postData);

        delete postData.revisions;

        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i]._hash;
            delete postData.detail[i].$$hashKey;
        }

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Quotation.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.setApproved = function(id) {
        this.setStatus(id, 'approve');
        this.create();
    };

    this.setRevision = function(id) {
        this.setStatus(id, 'revision');
        this.create();
    };

    this.setStatus = function(id, status) {
        vm.ls.get('loading-' + id).on();
        ApiService.Quotation.setStatus(id, status).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                vm.init();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading-' + id).off();
        });
    };

    this.sortByAngular = function(propertyName) {
        var sort = this.sort;

        if (sort.active == propertyName) {
            sort.descending = !sort.descending;
        } 
        else {
            sort.active = propertyName;
            sort.descending = false;
        }

        this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
        this.propertyName = propertyName;
    };

    this.getIconSort = function(column) {
    var sort = vm.sort;
    if (sort.active == column) {
        return sort.descending
        ? 'fa-sort-desc'
        : 'fa-sort-asc';
        }
    return 'fa-sort';
    };

    this.download = function(id){
        vm.ls.get('print').on();
        ApiService.Quotation.download(id).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);
                vm.ls.get('print').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

};
})();