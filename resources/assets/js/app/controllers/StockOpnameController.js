(function() {
'use strict';

angular.module('app').controller('StockOpnameController', StockOpnameController);

function StockOpnameController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.images = [];
    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    this.list = null;
    this.listPending = false;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            project_id: null,
            ref_no: null,
            created: DateFormatter.date(new Date()),
            is_stock: null,
            detail: [],
            status: {
                name: 'draft'
            },
            // List of detail id that want to be deleted
            _delete_detail: [],
            _delete_images: []
        };
    };

    this.init = function() {
        console.log('Stock Opname list init');
        var search = {};

        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.status == 'pending')
            this.listPending = true;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }
        this.isList = true;

        vm.search = search;

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        vm.ls.get('loading').on();
        if (this.listPending == true) {
            vm.search.status = 'pending';
        }
        console.log('do search', this.search);
        console.log(vm.search);
        ApiService.StockOpname.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    this.getStocks = function(project_id) {
        var params = {
            type: 'item',
            project_id: project_id
        };
        ApiService.StockOpname.stocks(params).then(function(resp){
            console.log(resp);
            vm.data.detail = resp.data.data;
            vm.data.detail.forEach(function(element) {
                element.qty_before = parseFloat(element.qty);
                element.qty_after = parseFloat(element.qty);
                element.qty = 0;
            });
        });
    };

    this.create = function() {
        this.createEmptyData();
        var id = $stateParams.id;
        if(id){
            vm.ls.get('loading').on();
            ApiService.StockOpname.get(id).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    var data = resp.data.data;

                    data._delete_detail = [];
                    data._delete_images = [];
                    vm.data = data;
                    vm.data.project_id = String(vm.data.project_id);

                    vm.data.detail.forEach(function(e) {
                        e.qty_after = (e.qty_before + e.qty);
                    });

                    vm.loadProject();
                    vm.ls.get('loading').off();
                } else {
                    vm.error.on();
                    vm.message = resp.data.errors;
                    $state.go('stock-opname');
                }
            });
        } else {
            // vm.getStocks(id);
        }
        this.loadProject();
    };

    this.loadProject = function() {
        ApiService.Project.all({page: 'all'}).then(function(resp) {
            var data = resp.data;
            vm.projects = data.data;
            for (var i = 0; i < vm.projects.length; i++) {
                vm.projects.id = String(vm.projects.id);
            }
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('stock-opname', this.search);
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        console.log(this.data);
        var postData = this._createPostData(this.data, this.images);

        if(vm.sendToManager == 1) {
            postData.data.status.name = 'pending';
        }
        vm.sendToManager = 0; // Set back to 0

        console.log('postData', postData);
        ApiService.StockOpname.create(postData).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                        $state.go('stock-opname-detail', { 'id': resp.data.data });
                    else
                        vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });

    };

    this._createPostData = function(data, images){
        var postData = JSON.parse(JSON.stringify(data));

        delete postData.revisions;
    
        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i].qty_after;

            delete postData.detail[i]._hash;
            delete postData.detail[i].$$hashKey;
        }
    
        return {data: postData, images: images};
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.StockOpname.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.deleteImage = function(image) {
        if(image.id)
            this.data._delete_images.push(image.id);

        this.data.images.removeByObject(image);
    }


    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.isDraft = function() {
        return this.isStatus('draft');
    };

    this.isApproved = function() {
        return this.isStatus('approve');
    };

    this.isDecline = function() {
        return this.isStatus('decline');
    };

    this.isPending = function() {
        return this.isStatus('pending');
    };

    this.isStatus = function(status) {
        return this.data.status.name === status;
    };

    this.setApproved = function(id) {
        this.setStatus(id, 'approve');
    };

    this.setDeclined = function(id) {
        this.setStatus(id, 'decline');
    };

    this.setStatus = function(id, status) {
        vm.ls.get('loading-' + id).on();
        ApiService.StockOpname.setStatus(id, status).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                if(vm.isList)
                    vm.init();
                else
                    vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading-' + id).off();
        });
    };
};
})();