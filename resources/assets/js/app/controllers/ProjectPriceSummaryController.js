(function() {
'use strict';

angular.module('app').controller('ProjectPriceSummaryController', ProjectPriceSummaryController);

function ProjectPriceSummaryController($window, $state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.editMode = false;
    this.project_id = $stateParams.project_id;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Store price plan detail data
    this.pricePlan = null;
    // Store project detail data
    this.project = null;

    this.createEmptyData = function() {
        return {
            id: null,
            name: null,
            customer_id: null,
            start: null,
            finish: null,
            ref_no: null,
            city_id: null,
            address: null,
            status_id: null,
            project: null,
            detail: [],
            revisions: [],
            _delete_detail: []
        };
    };

    this.isRevision = function() {
        return this.isStatus('revision');
    };

    this.setRevision = function(id) {
        this.setStatus(id, 'revision');
        this.create();
    };

    this.init = function() {
        console.log('Project list init');
        var search = {};

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.Project.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    // Redirect to correct route
    this.doSearch = function() {
        this.search.order_by = this.orderBy.toString();
        $state.go('project', this.search);
    };

    this.setHpp1 = function(quotation_id) {
        ApiService.Project.setHpp(this.parent_id, quotation_id).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    // $state.go('project-detail', { 'id': $stateParams.id });
                vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this.create = function() {
        var id = $stateParams.id;
        if(id) {
            vm.ls.get('loading').on();

            ApiService.Project.getPriceSummary(id).then(function(resp) {
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    //Set project properties
                    vm.project = data.project;
                    //Set summary properties
                    vm.summary = data;

                    vm.getProject(data.project_id, "detail");
                    vm.ls.get('loading').off();
                } else {
                    vm.message = resp.data.errors;
                    // $state.go('project-detail', {id: $stateParams.project_id});
                }
            });
        }
    };

    this.getProject = function(id, state){
        ApiService.Project.get(id).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                data.customer_id = String(data.customer_id);
                data.city_id = String(data.city_id);

                //Set project properties
                vm.project = data;
                vm.summary.project_id = data.id;

                if (state == null) {
                    if (data.price_plan_id) {
                        vm.getPricePlan(data.price_plan_id);   
                    }
                }
            } else {
                vm.message = resp.data.errors;
                $state.go('project-detail', {id: $stateParams.project_id});
            }
            vm.ls.get('loading').off();
        });
    };

    this.getPricePlan = function(id){
        var data = {
                quotation_id: id
        }
        ApiService.PricePlan.get(data).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                //Set project properties
                vm.project = data.project;
                //Set priceplan properties
                vm.pricePlan = data;

                vm.getProject(data.project_id, "detail");
                vm.ls.get('loading').off();
            } else {
                vm.message = resp.data.errors;
                $state.go('project-detail', {id: $stateParams.project_id});
            }
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('project', this.search);
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        var postData = this._createPostData();
        console.log(postData);
        ApiService.PricePlan.create(postData).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
                $state.go('price-plan-detail', { 'id': resp.data.data });
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this._createPostData = function(data){
        var pp = vm.pricePlan;
        var p = vm.project;

        // For temporary
        var postData = {
            id: pp.id,
            ref_no: pp.ref_no,
            project_id: p.id,
            detail: [],
            _delete_detail: []
        };

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Project.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.edit = function() {
        this.editMode = true;
    };

    this.isEditMode = function() {
        return this.editMode;
    };

    this.download = function(id){
        vm.ls.get('print').on();
        ApiService.PricePlan.download(id).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);
                vm.ls.get('print').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };
};
})();