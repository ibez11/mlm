(function() {

'use strict';

// Auth
angular.module('app').controller('ConfigController', ConfigController);

function ConfigController($stateParams, ApiService, StateFactory, FlashMessageService) {
  var $scope = this;

  this.loading = new StateFactory.Boolean;
  this.error = new StateFactory.Boolean;
  this.errorMessage = null;
  this.successMessage = null;
  this.fm = FlashMessageService;

  // Config list
  this.list = null;

  this.init = function() {
      this.loading.on();
      ApiService.Config.all().then(function(resp) {
          var data = resp.data;
          $scope.list = data.data;

          console.log($scope.list);
          $scope.loading.off();
      });
  };

  this.submit = function() {
      this.loading.on();
      //package param
      var tamp = {};
      for (var i = 0; i < $scope.list.length; i++) { 
          tamp[i] = {};
          tamp[i]['key'] = $scope.list[i].key;         
          tamp[i]['value'] = $scope.list[i].value;         
      }
      var param = {
        'data': tamp
      };

      ApiService.Config.add(param).then(function(resp) {
          console.log(resp);
          $scope.fm.success(resp.data.message);
          if(!resp.data.is_error) {
              var data = resp.data;
              $scope.list = data.data;
          }
          $scope.loading.off();
      });
  }
};

})();
