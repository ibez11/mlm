(function() {
    'use strict';
    
    angular.module('app').controller('ChecklistController', ChecklistController);
    
    function ChecklistController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
    
        // Paginator
        this.paginator = {};
        
        this.fm = FlashMessageService;
        this.ls = LoadingService;

        this.temp = null;
        this.list = [];
        this.data = {};
        // Project detail
        this.project = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;
    
        this.createEmptyData = function() {
            return {
                project_id: null,
                detail: [],
                _delete: [],
                _hash: Math.getRandomHash(),
            };
        };
    
        this.init = function() {
            console.log('Unit list init');
            this.createEmptyData();
            this.selectedHash = null;
            var search = {};
    
            // if($stateParams.created)
            //     search.created = $stateParams.created;
            // if($stateParams.status)
            //     search.status = $stateParams.status;
            search.group_by = 'unit';
    
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm.prop = 1;
            vm._doSearch();
        };
    
        // Search invoice
        this._doSearch = function() {
            vm.ls.get('loading').on();
            var id = $stateParams.project_id;
            // this.loadProject(id);
        };

        // Add blank category
        this.add = function() {
            this.temp = {
                id: null,
                name: 'test',
                position: '1',
                is_edit: true,
                _hash: Math.getRandomHash()
            };
            this.data.detail.push(this.temp);
        };
    
        this.create = function() {
            console.log('create');
            vm.ls.get('loading').on();
            var id = $stateParams.project_id;
            this.data = this.createEmptyData();
            if(id){
                vm.ls.get('loading').on();
                ApiService.Project.Checklist.detail(id).then(function(resp) {
                    console.log('resp',resp);
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        data.project_id = String(data.project_id);
                        vm.data = data;
                        data._delete_detail = [];
                        console.log('ajax detail', vm.data);
                        vm.ls.get('loading').off();
                    } else {
                        vm.fm.error(resp.data.message);
                        $state.go('project'); 
                        vm.ls.get('loading').off();
                    }
                });
            }
            vm.ls.get('loading-hpp').off();
        };

        
        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();

            var postData = this._createPostData(this.data);
            console.log('post data', postData);
            ApiService.Project.Checklist.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    if(!$stateParams.id) {
                        vm.loadProject(resp.data.data);
                        vm.fm.success(resp.data.message);
                    }else {
                        vm.create();

                    vm.fm.success(resp.data.message);
                    }
                } else {
                    vm.fm.error(resp.data.message);
                }
                console.log(resp);
                vm.ls.get('loading').off();
            });
        };

        this.loadProject = function(id) {
            vm.ls.get('loading').on();
            ApiService.Project.Checklist.detail(id).then(function(resp) {
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    data.project_id = String(data.project_id);
                    vm.data = data;
                    data._delete_detail = [];
                    vm.ls.get('loading').off();
                } else {
                    vm.fm.error(resp.data.message);
                    $state.go('project'); 
                    vm.ls.get('loading').off();
                }
            });
        };
    
        this._createPostData = function(data){
            var postData = JSON.parse(JSON.stringify(data));

            console.log("Data : " + postData);

            for(var i=0; i<postData.detail.length; i++) {
                delete postData.detail[i]._hash;
                delete postData.detail[i].is_edit;
                delete postData.detail[i].$$hashKey;
            }

            return postData;
        };
    
        this.delete = function(checklist) {
            if(checklist.id) {
                this.data._delete_detail.push(checklist.id);
            } 

            this.data.detail.removeByObject(checklist);
        };

        // Redirect to correct route
        this.doSearch = function() {
            console.log('search', this.search);
            this.search.order_by = this.orderBy.toString();
            $state.go('category', this.search);
        };

        // Edit unit
        this.edit = function(unit) {
            vm.selectedHash = unit._hash;
        };
  
        
        this.sortBy = function(columnName) {
            vm.reverse = (vm.propertyName === columnName) ? !vm.reverse : false;
            vm.propertyName = columnName;
            
            // this.orderBy.setColumn(columnName);
    
            // Generate search params
            // this.search.order_by = this.orderBy.toString();
            // this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();