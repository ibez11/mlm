(function() {

'use strict';

// Auth
angular.module('app').controller('DashboardController', DashboardController);

function DashboardController($stateParams, ApiService, StateFactory, FlashMessageService) {
  var $scope = this;

  this.loading = new StateFactory.Boolean;
  this.error = new StateFactory.Boolean;
  this.errorMessage = null;
  this.successMessage = null;
  this.fm = FlashMessageService;

  // list
  this.year = null;
  this.overview = null;
  this.orderUnarchived = null;
  this.topOrder = null;
  this.compareInvoice = null;
  this.revenueMonthly = null;

  // param
  this.param = {
    year: null
  };

  this.init = function() {
      console.log('init dashboard');
      var year = new Date().getFullYear();
      this.year = [];
      this.year.push(year);
      for (var i = 1; i < 5; i++) {
          this.year.push(year - i);
      }

      ApiService.Dashboard.status().then(function(resp) {
          var data = resp.data;
          console.log('status', data);
          $scope.overview = data.data;
      });

      ApiService.Order.search({status: 0}).then(function(resp) {
          var data = resp.data;
          console.log('orderUnarchived', data);
          $scope.orderUnarchived = data.data;
      });

      ApiService.Dashboard.topOrder().then(function(resp) {
          var data = resp.data;
          console.log('topOrder', data);
          $scope.topOrder = data.data;
      });

      ApiService.Dashboard.compareInvoice().then(function(resp) {
          var data = resp.data;
          console.log('compareInvoice', data);
          $scope.compareInvoice = data.data;
          $scope.setChartArea();
      });
      
      //default value
      this.param.year = '2018';
      $scope.getRevenueMonthly();
  };

  this.getRevenueMonthly = function() {
      this.loading.on();
      ApiService.Dashboard.revenueMonthly({tahun: $scope.param.year}).then(function(resp) {
          var data = resp.data;
          console.log('revenueMonthly', data);
          $scope.revenueMonthly = data.data;
          $scope.loading.off();
      });
  };

  this.setChartArea = function() {
      console.log('moris', $scope.compareInvoice);
      new Morris.Area({
        // ID of the element in which to draw the chart.
        element: 'areaChart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: $scope.compareInvoice,
        // The name of the data record attribute that contains x-values.
        xkey: 'periode',
        // A list of names of data record attributes that contain y-values.
        ykeys: ['nota_kirim', 'nota_bahan'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Nota Kirim', 'Nota Bahan']
      });
  };
};

})();
