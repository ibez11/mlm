(function() {
'use strict';

angular.module('app').controller('JobsController', JobsController);

function JobsController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.MAX_WORKER = 3;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;
    this._delete = [];
    this.parent_id = $stateParams.id;

    this.quotationTotal = 0; // Parent total

    this.item_category = 'item';
    this.total = 0;

    this.reverse = true;
    this.propertyName = 'name';

    this.message = null;
    this.orderBy = new OrderBy();
    this.search = {};
    this.sort = {
        active: '',
        descending: undefined
    }

    this.tempJob = {
            id: null,
            item_id: null,
            item_id_detail: '',
            item_category_id: 'item',
            item_category_name: 'Bahan',
            name: null,
            unit: '?',
            qty: 0,
            price: 0
    };

    this.createEmptyData = function() {
        this.data = {
            id: null,
            name: null,
            ref_no: null,
            detail: [],
            _delete_detail: []
        };
    };


    this.init = function() {

        var search = vm.search;
    
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
            
        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];
    
            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }
    
        vm._doSearch();
    };

    this._doSearch = function() {
        vm.ls.get('loading').on();
        console.log(vm.search);
        ApiService.Jobs.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;
            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }
    
                // Paginator
            vm.paginator = Paginator(data);
    
            vm.ls.get('loading').off();
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        this.search.order_by = this.orderBy.toString();
        $state.go('jobs', this.search);    
        
    };

    this.create = function() {
        this.createEmptyData();
        var id = $stateParams.id;
        if (id) {
            vm.ls.get('loading').on();
            ApiService.Jobs.get(id).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {

                    vm.loadUnit();

                    var data = resp.data.data;
                    data._delete_detail = [];
                    vm.data = data;
                    for(var i=0; i<vm.data.detail.length; i++) {
                        vm.data.detail[i].item_id_detail = String(vm.data.detail[i].item_id_detail);
                        vm.data.detail[i]._hash = vm._getRandomHash();
                    }
                    vm.countTotal();
                    vm.ls.get('loading').off();
                } else {
                    vm.message = resp.data.errors;
                    // $state.go('jobs-detail', {id: $stateParams.project_id});
                }
            });
        }
        this.loadItems('item');
        this.loadUnit();
    };

    this.loadUnit = function() {
        var search = {
            group_by: 'unit'
        };

        ApiService.Category.all(search).then(function(resp) {
            var data = resp.data;
            vm.units = data.data;
            vm.units.id = String(vm.units.id);
        });
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        var postData = this._createPostData(this.data);

        if(vm.sendToManager == 1) {
            postData.status = 'pending';
        }
        console.log(postData);
        vm.sendToManager = 0; // Set back to 0
        ApiService.Jobs.create(postData).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('jobs-detail', { 'id': resp.data.data });
                else
                    vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            vm.ls.get('loading').off();
        });

    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Jobs.delete(id).then(function(resp) {

            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this._createPostData = function(data){
        var postData = JSON.parse(JSON.stringify(data));

        delete postData.revisions;

        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i]._hash;
            delete postData.detail[i].$$hashKey;
        }

        return postData;
    };

    // Load item based on category
    // @param string item, vendor, mandor
    this.loadItems = function(data){
        vm.ls.get('loading-item').on();
        vm.item_category = data;
        ApiService.Item.all({item: data, page: 'all'}).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                for (var i = 0; i < data.length; i++) {
                    data[i].id = String(data[i].id);
                }
                vm.items = data;
                vm.ls.get('loading-item').off();
            } else {
                vm.message = resp.data.errors;
            }
        });
        this.tempJob.item_category_id = data;
    };

    this._getRandomHash = function() {
        var s1, s2;
        s1 = String(Math.floor(Math.random()*1000));
        s2 = String(new Date().getTime());
        return s1 + s2;
    };

    this.newJob = function() {
        this.tempJob = {
            id: null,
            item_id: null,
            item_id_detail: '',
            item_category_id: 'item',
            item_category_name: 'Bahan',
            name: null,
            unit: '?',
            qty: 0,
            price: 0,
            _hash: this._getRandomHash()
        };
        this.item_category = 'item';
    };

    this.addJob = function() {
        var isNew = true;

        // Check if this job update
        for(var i=0; i<this.data.detail.length; i++) {
            if(this.data.detail[i]._hash === this.tempJob._hash) {
                isNew = false;
            }
        }

        for(var i=0; i<this.data.detail.length; i++) {
            if(this.data.detail[i].item_id_detail === this.tempJob.item_id_detail) {
                isNew = false;
                this.data.detail[i].qty = this.tempJob.qty;
            }
        }

        if(isNew){
            this.changeLabel();
            this.data.detail.push(this.tempJob);
        }

        vm.countTotal();
        this.newJob();
        return;
    };

    this.changeLabel = function() {
        console.log(this.tempJob);
        if (this.tempJob.item_category_id === 'item') {
            this.tempJob.item_category_name = 'Bahan';
        }

        if (this.tempJob.item_category_id === 'vendor') {
            this.tempJob.item_category_name = 'Vendor';
        }

        if (this.tempJob.item_category_id === 'mandor') {
            this.tempJob.item_category_name = 'Mandor';
        }
    };

    // Grouping quote details
    this.countTotal = function() {
        var total = 0;

        for(var i=0; i<this.data.detail.length; i++) {
            var data = this.data.detail[i];
            var totalItem = data.qty * data.price;
            total += totalItem;
        }
        vm.total = total;
    };

    this.setItem = function(itemId) {
        var x = null;
        for(var i=0; i<this.items.length; i++) {
            x = this.items[i];
            if(x.id == itemId) {
                vm.tempJob.item_id_detail = String(x.id);
                vm.tempJob.name = x.name;
                vm.tempJob.qty = x.qty;
                vm.tempJob.price = x.price;
                vm.tempJob.unit = x.unit;
            }
        }
    };

    this.deleteJob = function(item) {
        if(item.id)
            this.data._delete_detail.push(item.id);
    
        this.data.detail.removeByObject(item);

        vm.countTotal();
    }

    this.editJob = function(job) {
        this.tempJob = job;
        this.tempJob.item_id_detail = String(this.tempJob.item_id_detail);

        console.log('job', this.tempJob);
        vm.loadItems(this.tempJob.item_category_id);
    };

    this.sortByAngular = function(propertyName) {
        var sort = this.sort;

        if (sort.active == propertyName) {
            sort.descending = !sort.descending;
        }
        else {
            sort.active = propertyName;
            sort.descending = false;
        }

        this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
        this.propertyName = propertyName;
    };

    this.getIconSort = function(column) {
    var sort = vm.sort;
    if (sort.active == column) {
        return sort.descending
        ? 'fa-sort-desc'
        : 'fa-sort-asc';
        }
    return 'fa-sort';
    };

};
})();