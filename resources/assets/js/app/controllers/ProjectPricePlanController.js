(function() {
'use strict';

angular.module('app').controller('ProjectPricePlanController', ProjectPricePlanController);

function ProjectPricePlanController($window, $state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.editMode = false;
    this.project_id = $stateParams.project_id;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;
    this.setStatus = null;

    // Store price plan detail data
    this.pricePlan = null;
    // Store project detail data
    this.project = null;
    this.listPending = false;

    this.createEmptyData = function() {
        return {
            id: null,
            name: null,
            customer_id: null,
            start: null,
            finish: null,
            ref_no: null,
            city_id: null,
            address: null,
            status: {
                name: 'draft'
            },
            project: null,
            detail: [],
            revisions: [],
            _delete_detail: []
        };
    };

    this.init = function() {
        console.log('Project list init');
        var search = {};

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.status_page == 'pending')
            this.listPending = true;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    this.createRevision = function() {
        var id = $stateParams.id;

        vm.ls.get('revision').on();
        ApiService.Quotation.createRevision(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm.ls.get('revision').off();
                vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
        });
    };

    // Search invoice
    this._doSearch = function() {
        if (this.listPending == true) {
            vm.search.status = 'pending';
        }
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.PricePlan.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        if(this.listPending == true){
            this.search.order_by = this.orderBy.toString();
            this.search.status_page = 'pending';
            $state.go('price-plan-pending', this.search);
        } else {
            this.search.order_by = this.orderBy.toString();
            $state.go('price-plan', this.search);
        }
    };

    this.setHpp1 = function(quotation_id) {
        ApiService.Project.setHpp(this.parent_id, quotation_id).then(function(resp) {
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    // $state.go('project-detail', { 'id': $stateParams.id });
                vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this.create = function() {
        vm.ls.get('loading').on();
        let stateName = $state.current.name;
        // Check if this create or edit
        if (!$stateParams.project_id) {
            // This mean edit or detail
            // Just load price plan detail
            this.pricePlan = this.createEmptyData();
            var id = $stateParams.id;
            if(stateName == 'price-plan-detail'){
                this.getPricePlan(id);
            }
            if(stateName == 'hpp3-detail'){
                this.getHPP3(id);
            }
        } else {
            // Init empty project plan
            this.pricePlan = this.createEmptyData();
            var projectId = $stateParams.project_id;

            if (stateName == 'project-price-plan') {
                this.getProject(projectId, stateName);
            }
        }
    };

    this.getProject = function(id, state){
        ApiService.Project.get(id).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                data.customer_id = String(data.customer_id);
                data.city_id = String(data.city_id);

                //Set project properties
                vm.project = data;
                vm.pricePlan.project_id = data.id;

                if (state == null) {
                    if (data.price_plan_id) {
                        vm.getPricePlan(data.price_plan_id);
                    }
                }

                if (state == 'project-price-plan') {
                    vm.getQuotation(vm.project.quotation_id);
                }

            } else {
                vm.message = resp.data.errors;
                $state.go('price-plan-detail', {id: $stateParams.project_id});
            }
        });
    };

    this.getQuotation = function(id){
        ApiService.Quotation.get(id).then(function(resp) {
            var data = resp.data;
            vm.pricePlan = data.data;
            vm.pricePlan.id = null;
            vm.pricePlan.ref_no = null;
            vm.pricePlan.is_quotation = 0;
            vm.pricePlan.quotation_id = null;
            vm.pricePlan.status = {
                name: 'draft'
            };
            vm.pricePlan.revisions = [];

            vm.pricePlan._delete_detail = [];

            for (var i = 0; i < vm.pricePlan.detail.length; i++) {
                vm.pricePlan.detail[i]._delete_detail = [];
            }

            console.log(vm.pricePlan);
        });
    };

    this.getPricePlan = function(id){
        var data = {
                quotation_id: id
        }
        ApiService.PricePlan.get(data).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                var data = resp.data.data;
                //Set project properties
                vm.project = data.project;
                //Set priceplan properties
                vm.pricePlan = data;
                vm.groups = vm._groupingDetail(data.detail);
                console.log('groups', vm.groups);

                vm.pricePlan._delete_detail = [];

                for (var i = 0; i < vm.pricePlan.detail.length; i++) {
                    vm.pricePlan.detail[i]._delete_detail = [];
                }

                vm.getProject(data.project_id, "detail");
                vm.ls.get('loading').off();
            } else {
                vm.message = resp.data.errors;
                $state.go('price-plan-detail', {id: $stateParams.project_id});
            }
        });

        this.loadJobs();
        this.loadUnit();
    };

    this.getHPP3 = function(id){
        // var data = {
        //         quotation_id: id
        // }
        ApiService.Project.getHPP3(id).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                //Set project properties
                vm.project = data.project;
                //Set priceplan properties
                vm.pricePlan = data;

                vm.getProject(data.project_id, "detail");
                vm.ls.get('loading').off();
            } else {
                vm.message = resp.data.errors;
                $state.go('project-detail', {id: $stateParams.project_id});
            }
        });
    };

    // Grouping quote details
    this._groupingDetail = function(details) {
        var profit = parseFloat(vm.pricePlan.profit);
        var risk_factor = parseFloat(vm.pricePlan.risk_factor);

        var percent = profit + risk_factor;

        for (var i = 0; i < details.length; i++) {
            var total = details[i].price * (percent / 100);
            details[i].price_profit = parseFloat(details[i].price) + total;
        }

        var gd = new GroupingDetail(details);

        return gd.get();
    };

    this.newJob = function() {
        this.tempJob = {
            item_id: '',
            qty: 0,
            unit: '?',
            price: 0,
            price_profit: 0,
            _hash: Math.getRandomHash()
        };
    };

    this.loadJobs = function() {
        var search = {
            page: 'all'
        }
        ApiService.Jobs.all(search).then(function(resp) {
            var data = resp.data;
            vm.jobs = data.data;
            for (var i = 0; i < vm.jobs.length; i++) {
                vm.jobs[i].id = String(vm.jobs[i].id);
            }
        });
    };

    this.loadUnit = function() {
        var search = {
            group_by: 'unit',
            page: 'all'
        };

        ApiService.Category.all(search).then(function(resp) {
            var data = resp.data;
            vm.units = data.data;
        });
    };

    this.loadJobDetail = function(id) {
        vm.ls.get('job').on();
        ApiService.Jobs.get(id).then(function(resp) {
            var data = resp.data;
            vm.tempJob = data.data;
            vm.tempJob.item_id = String(data.data.id);
            vm.tempJob.id = null;
            vm.tempJob.qty = 0;
            vm.tempJob.price = 0;
            for (var i = 0; i < vm.tempJob.detail.length; i++) {
                vm.tempJob.detail[i].price_profit = 0; // vm.countProfitDetail(vm.tempJob.detail[i]);
                var total = vm.tempJob.detail[i].qty * vm.tempJob.detail[i].price;
                vm.tempJob.detail[i].id = null;
                vm.tempJob.detail[i].item_id = vm.tempJob.detail[i].item_id_detail;
                vm.tempJob.price += total;
            }
            vm.tempJob.price_profit = vm.tempJob.price;
            vm.tempJob._hash = Math.getRandomHash();
            vm.ls.get('job').off();
        });
    };

    this.countTotalDetail = function() {
        vm.tempJob.price = 0;

        for (var i = 0; i < vm.tempJob.detail.length; i++) {
            var total = vm.tempJob.detail[i].qty * vm.tempJob.detail[i].price;
            vm.tempJob.price += total;

            console.log('total', total);
        }
        // vm.tempJob.price_profit = vm.tempJob.price;
    };

    this.addJob = function() {
        var isNew = true;
        this.data = this.pricePlan;
        console.log('add job', this.data, this.tempJob);

        // Check if this job update
        for(var i=0; i<this.data.detail.length; i++) {
            if(this.data.detail[i]._hash === this.tempJob._hash) {
                this.tempJob.price_profit = this.tempJob.price;
                this.data.detail[i] = this.tempJob;
                isNew = false;
            }
        }

        if(isNew){
            this.tempJob.price_profit = this.tempJob.price;
            // If new just push to detail
            var isError = false;
            for (var i = 0; i < this.data.detail.length; i++) {
                if (this.tempJob.item_id == this.data.detail[i].item_id) {
                    isError = true;
                    vm.fm.error('Pekerjaan sudah ada.');
                }
            }
            if (!isError && this.tempJob.item_id) {
                this.data.detail.push(this.tempJob);
            }
        }

        // and rerender it
        this.groups = this._groupingDetail(this.data.detail);
        this.newJob();
        this.countAll();
        return;
    };

    this.deleteJob = function(job) {
        this.data = this.pricePlan;

        if(job.id)
            this.data._delete_detail.push(job.id);

        this.data.detail.removeByObject(job);
        this.groups = this._groupingDetail(this.data.detail);
        // this.countAll();

        // console.log('delete job', this.data, job, this.pricePlan);
    }

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        var postData = this._createPostData();

        if(vm.sendToManager == 1) {
            postData.status.name = 'pending';
        }
        vm.sendToManager = 0; // Set back to 0

        console.log('post data', postData);
        ApiService.PricePlan.create(postData).then(function(resp){
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
                vm.create();
                $state.go('price-plan-detail', { 'id': resp.data.data });
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this._createPostData = function(data){
        var pp = vm.pricePlan;
        var p = vm.project;

        // For temporary
        var postData = {
            id: pp.id,
            ref_no: pp.ref_no,
            project_id: p.id,
            status: pp.status,
            detail: pp.detail,
            address: pp.address,
            city_id: pp.city_id,
            customer_id: pp.customer_id,
            quotation_id: pp.quotation_id,
            schedule_status_id: pp.schedule_status_id,
            is_quotation: 0,
            is_tax: pp.is_tax,
            name: pp.name,
            profit: pp.profit,
            risk_factor: pp.risk_factor,
            pph: pp.pph,
            created: pp.created,
            _delete_detail: pp._delete_detail
        };

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Project.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.edit = function() {
        this.editMode = true;
    };

    this.editJob = function(job) {
        console.log('job', job);
        console.log('jobs', vm.jobs);
        this.tempJob = job;
    };

    this.isEditMode = function() {
        return this.editMode;
    };

    this.download = function(id){
        vm.ls.get('print').on();
        ApiService.PricePlan.download(id).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);
                vm.ls.get('print').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

    this.isCreate = function() {
        return !$stateParams.id;
    };

    this.isDraft = function() {
        return this.isStatus('draft');
    };

    this.isPending = function() {
        return this.isStatus('pending');
    };

    this.isRevision = function() {
        return this.isStatus('revision');
    };

    this.isStatus = function(status) {
        return this.pricePlan.status.name === status;
    };

    this.setApproved = function(id) {
        this.setStatus(id, 'approve');
    };

    this.setDraft = function(id) {
        this.setStatus(id, 'draft');
        this.create();
    };

    this.setPending = function(id) {
        this.setStatus(id, 'pending');
        this.create();
    };

    this.setRevision = function(id) {
        this.setStatus(id, 'revision');
    };

    this.setStatus = function(id, status) {
        vm.ls.get('loading-' + id).on();
        ApiService.PricePlan.setStatus(id, status).then(function(resp) {
            if(!resp.data.is_error) {
                //For refresh / re-init
                if (vm.listPending == true) {
                    vm.init();
                } else {
                    vm.create();
                }
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading-' + id).off();
        });
    };

    this.changeJobs = function(id) {
        var params = {
            quotation_id: vm.pricePlan.id,
            quotation_detail_id: id
        };
        if (vm.pricePlan.id != null) {
            $state.go('price-plan-change-jobs', params);
        } else {
            vm.fm.error('HPP 2 harus disimpan dulu sebelum dapat mengubah detail pekerjaan');
        }
    };

};
})();