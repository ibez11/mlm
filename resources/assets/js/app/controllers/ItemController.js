(function() {
'use strict';

angular.module('app').controller('ItemController', ItemController);

function ItemController($state, $window, $stateParams, ApiService, LoadingService, StateFactory,
FlashMessageService) {
    var vm = this;
    var $scope = this;

    this.permissions = {
        supplier: ['item_create', 'item_delete', 'item_detail', 'item_list'],
        mandor: ['item_mandor_create', 'item_mandor_delete', 'item_mandor_detail',
            'item_mandor_list'],
        vendor: ['item_vendor_create', 'item_vendor_delete', 'item_vendor_detail',
            'item_vendor_list']
    };
    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.myOrderBy = null;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;
    this.fileImport = [];

    this.createEmptyData = function() {
        this.data = {
            id: null,
            item_category_id: null,
            user_id: null,
            ref_no: null,
            name: null,
            unit: null,
            price: null,
            created_at: null,
            updated_at: null
        };
    };

    this.searchHistory = {};

    this.globalId = null;

    this.titleRole = null;

    this.role = $stateParams.role;

    this.setTitle = function() {
        if (this.role == "supplier") {
            this.titleRole = "List Bahan";
        } else if (this.role == "mandor") {
            this.titleRole = "List Jasa Mandor";
        } else if (this.role == "vendor") {
            this.titleRole = "List Jasa Vendor";
        }
    };

    this.init = function() {
        this.setTitle();
        var search = {};

        // Mapping role to item category
        if($stateParams.role) {
            var role = $stateParams.role;
            switch(role) {
                case 'supplier':
                    search.item = 'item';
                    break;
                default:
                    search.item = role;
                    break;
            }
        }

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    // Get correct permission from string based on role
    this.getPermission = function(permission) {
        var permissionTable = this.permissions.supplier;
        switch(this.role) {
            case 'mandor':
                permissionTable = this.permissions.mandor;
                break;
            case 'vendor':
                permissionTable = this.permissions.vendor;
                break;
        }

        var newPermission = null;
        switch(permission) {
            case 'create':
                newPermission = permissionTable[0];
                break;
            case 'detail':
                newPermission = permissionTable[2];
                break;
            case 'delete':
                newPermission = permissionTable[1];
                break;
            case 'list':
                newPermission = permissionTable[3];
                break;
        }

        return newPermission;
    }

    // Search invoice
    this._doSearch = function() {
        this.ls.get('loading').on();
        console.log(vm.search);
        ApiService.Item.all(vm.search).then(function(resp) {
            var data = resp.data;
            data.price = parseInt(data.price);

            vm.list = data.data;
            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    // Redirect to correct route
    this.doSearch = function() {
        this.search.order_by = this.orderBy.toString();
        $state.go('item', this.search);
    };

    // Call History
    this.callHistory = function() {
        // Load History Harga list
        this.ls.get('loading-history').on();
        var params = {
            item_id: this.globalId
        };
        ApiService.PriceHistory.all(params).then(function(resp) {
            console.log(resp);
            var data = resp.data;
            vm.history = data.data;

            // // Paginator
            vm.paginator = Paginator(data);
            vm.ls.get('loading-history').off();
        });
    };

    this.create = function() {
        this.setTitle();
        this.createEmptyData();
        var id = $stateParams.id;
        this.globalId = $stateParams.id;
        if(id){
            this.ls.get('loading').on();
            ApiService.Item.get(id).then(function(resp) {
                vm.loadUnit();
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    data.user_id = String(data.user_id);

                    vm.data = data;
                } else {
                    // vm.error.on();
                    vm.message = resp.data.errors;
                }
                vm.ls.get('loading').off();
            });
            this.callHistory();
        }

        this.loadUnit();

        // Load Supplier list
        var search = {
            'roles': this.role,
            page: 'all'
        };
        ApiService.Admin.all(search).then(function(resp) {
            var data = resp.data;
            data.id = String(data.id);
            vm.suppliers = data.data;
        });
    };

    this.loadUnit = function() {
        var search = {
            group_by: 'unit'
        };

        ApiService.Category.all(search).then(function(resp) {
            var data = resp.data;
            vm.units = data.data;
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('item', this.search);
    };

    this.historyPaging = function(page) {
        // Load History Harga list
        
        this.ls.get('loading-history').on();
            var params = {
                page: page,
                item_id: this.globalId
            };
            ApiService.PriceHistory.all(params).then(function(resp) {
                var data = resp.data;
                vm.history = data.data;

                // // Paginator
                vm.paginator = Paginator(data);
                vm.ls.get('loading-history').off();
            });
    };

    // Submit to DB
    this.submit = function(){
        this.ls.get('loading').on();
        this.data.role = this.role;
        console.log(this.data);
        ApiService.Item.add(this.data).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('item-detail', { 'id': resp.data.data.id, 'role':resp.data.data.role });
                else
                    vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            vm.ls.get('loading').off();
        });
    };

    this.delete = function(id) {
        this.ls.get('loading').on();
        ApiService.Item.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.download = function(){
        vm.ls.get('download').on();
        var role = this.role;

        if (role == 'supplier') {
            role = 'item';
        }

        var params = {
            role: role
        };
        ApiService.Item.download(params).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);

                vm.fm.success(resp.data.message);
                vm.ls.get('download').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

    this.export = function(){
        vm.ls.get('export').on();
        var role = this.role;

        if (role == 'supplier') {
            role = 'item';
        }

        var params = {
            role: role
        };
        ApiService.Item.export(params).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);

                vm.fm.success(resp.data.message);
                vm.ls.get('export').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

    this.import = function(){
        vm.ls.get('import').on();
        var role = this.role;
        var file = this.fileImport;

        if (role == 'supplier') {
            role = 'item';
        }

        var params = {
            role: role,
            file: file
        };

        ApiService.Item.import(params).then(function(resp) {
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
                vm.ls.get('import').off();
                vm.init();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

};
})();