(function() {

'use strict';

angular.module('app.service').service('ApiService', ApiService);

function ApiService(
  $http, $httpParamSerializer, ApiTokenService) {
  var apiToken = ApiTokenService.get();

  this.Dashboard = {};

  this.Quotation = {
      all: function(params) {
        params.api_token = apiToken;
        var url = '/api/quotation/list?' + $.param(params);

        return $http.get(url);
      },
      create: function(params) {
        // Because max_input_vars, so convert to json to make input_vars = 2 only
        var newParams = {
            api_token: apiToken,
            data: JSON.stringify(params)
        }
        // params.api_token = apiToken;
        var url = '/api/quotation/create';

        return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(newParams)
        });
      },
      get: function(id){
      var url = '/api/quotation/detail/' + id + '?api_token=' + apiToken;

      return $http.get(url);
      },
      delete: function(id){
        var url = '/api/quotation/' + id + '?api_token=' + apiToken;

        return $http.delete(url);
      },
      setStatus: function(id, status){
      var url = '/api/quotation/' + id + '/set-status/' + status +'?api_token=' + apiToken;

      return $http.get(url);
      },
      download: function(params) {
        params.api_token = apiToken;
        var url = '/api/quotation/download/' + params.id + '?' + $.param(params);

        return $http.get(url);
      },
      downloadSchedule: function(id){
      var url = '/api/quotation/download/schedule/' + id + '?api_token=' + apiToken;

      return $http.get(url);
      },
      schedule: function(params) {
        // Because max_input_vars, so convert to json to make input_vars = 2 only
        var newParams = {
            api_token: apiToken,
            data: JSON.stringify(params)
        }
        // params.api_token = apiToken;
        var url = '/api/quotation/schedule/create';

        return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(newParams)
        });
      },
      scheduleList: function(params) {
        params.api_token = apiToken;
        var url = '/api/schedule/list?' + $.param(params);

        return $http.get(url);
      },
      setStatusSchedule: function(id, status){
      var url = '/api/quotation/' + id + '/set-status/schedule/' + status +'?api_token=' + apiToken;

      return $http.get(url);
      },
      deleteSchedule: function(id){
        var url = '/api/schedule/' + id + '?api_token=' + apiToken;

        return $http.delete(url);
      },
      createRevision: function(id){
      var url = '/api/quotation/create-revision/' + id + '?api_token=' + apiToken;

      return $http.get(url);
      }
  };

  this.PricePlan = {
    all: function(params) {
        params.api_token = apiToken;
        var url = '/api/price-plan/list?' + $.param(params);

        return $http.get(url);
    },
    create: function(params) {
      // Because max_input_vars, so convert to json to make input_vars = 2 only
      var newParams = {
          api_token: apiToken,
          data: JSON.stringify(params)
      }

      var url = '/api/price-plan/create';

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(newParams)
      });
    },
    get: function(params){
      params.api_token = apiToken;
      var url = '/api/price-plan/detail/' + params.quotation_id + '?' + $.param(params);

      return $http.get(url);
    },
    download: function(id){
      var url = '/api/price-plan/download/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    setStatus: function(id, status){
      var url = '/api/price-plan/' + id + '/set-status/' + status +'?api_token=' + apiToken;

      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/price-plan/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    }
  };


    this.PurchaseOrder = {
        all: function(params) {
            params.api_token = apiToken;
            var url = '/api/purchase-order/list?' + $.param(params);

            return $http.get(url);
        },
        create: function(params) {
            // Because max_input_vars, so convert to json to make input_vars = 2 only
            var newParams = {
                api_token: apiToken,
                data: JSON.stringify(params)
            }
            // params.api_token = apiToken;
            var url = '/api/purchase-order/create';

            return $http({
            method: 'POST',
            url:url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(newParams)
            });
        },
        get: function(id){
        var url = '/api/purchase-order/detail/' + id + '?api_token=' + apiToken;

        return $http.get(url);
        },
        delete: function(id){
            var url = '/api/purchase-order/' + id + '?api_token=' + apiToken;

            return $http.delete(url);
        },
        setStatus: function(id, status){
            var url = '/api/purchase-order/' + id + '/set-status/' + status +'?api_token=' + apiToken;

            return $http.get(url);
        },
        setTax: function(id, tax){
            var url = '/api/purchase-order/' + id + '/set-tax/' + tax +'?api_token=' + apiToken;

            return $http.get(url);
        },
        itemListAvailable: function(id) {
            var url = '/api/purchase-order/' + id + '/item/list/available?api_token=' + apiToken;

            return $http.get(url);
        },
        downloadSp: function(id){
            var url = '/api/purchase-order/download-sp/' + id + '?api_token=' + apiToken;

            return $http.get(url);
        },
        downloadPo: function(id){
            var url = '/api/purchase-order/download-po/' + id + '?api_token=' + apiToken;

            return $http.get(url);
        },
        Payment: {
            all: function(params) {
              params.api_token = apiToken;
              var url = '/api/purchase-order/payment/list?' + $.param(params);

              return $http.get(url);
          },
          get: function(id){
            var url = '/api/purchase-order/payment/detail/' + id + '?api_token=' + apiToken;
    
            return $http.get(url);
            },
          add: function(params) {
            params.api_token = apiToken;
            // params.api_token = apiToken;
            var url = '/api/purchase-order/payment/create?api_token=' + apiToken;
      
            return $http({
                method: 'POST',
                url:url,
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(params)
            });
          },
          delete: function(id) {
            var url = '/api/purchase-order/payment/' + id + '?api_token=' + apiToken;
      
            return $http.delete(url);
          }

        }
    };

  this.UserRole = {
    all: function(params) {
      var url = '/api/admin/list?type=' + params + '&api_token=' + apiToken;

      return $http.get(url);
    },
    get: function(id) {
      var url = '/api/role/detail/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    getPermisson: function(id) {
      var url = '/api/role/' + id + '/permission?api_token=' + apiToken;

      return $http.get(url);
    },
    create: function(params) {
      params.api_token = apiToken;

      console.log('api', params);

      // params.api_token = apiToken;
      var url = '/api/role/create';
      // var url = '/api/project/checklist/create';

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    setPermission: function(params) {
      params.api_token = apiToken;

      console.log('api', params);

      // params.api_token = apiToken;
      var url = '/api/role/set-permission';
      // var url = '/api/project/checklist/create';

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
  };

  this.Customer = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/customer/list?' + $.param(params);

      return $http.get(url);
    },
    get: function(id) {
      var url = '/api/customer/detail/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    add: function(post) {
      var params = post;
      var url = '/api/customer/create?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    delete: function(id){
      var url = '/api/customer/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    }
  };

  this.Item = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/item/list?' + $.param(params);

      return $http.get(url);
    },
    add: function(post) {
      var params = post;
      var url = '/api/item/create?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    get: function(id) {
      var url = '/api/item/detail/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/item/delete/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    download: function(params) {
      params.api_token = apiToken;
      var url = '/api/item/download?' + $.param(params);

      return $http.get(url);
    },
    export: function(params) {
      params.api_token = apiToken;
      var url = '/api/item/export?' + $.param(params);

      return $http.get(url);
    },
    import: function(params) {
      console.log('create', params);

      var formData = new FormData();

      formData.append('api_token', apiToken);
      formData.append('role', params.role);
      for(var i=0; i<params.file.length; i++) {
          formData.append('file[]', params.file[i]);
      }

      var url = '/api/item/import';

      return $http({
          method: 'POST',
          url:url,
          headers: {
              'Content-Type': undefined,
          },
          data: formData
      });
    }
  };

  this.PriceHistory = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/item/price-history/list?' + $.param(params);

      return $http.get(url);
    },
    get: function(item_id) {
      var url = '/api/item/price-history/' + item_id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    page: function(page, id) {
      var url = '/api/item/price-history/' + id + '?page=' + page + '&api_token=' + apiToken;

      return $http.get(url);
    },
    add: function(post) {
      var params = post;
      var url = '/api/item/price-history?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    delete: function(id) {
      var url = '/api/item/price-history/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    }
  };

  this.City = {
    all: function() {
      var url = '/api/city?api_token=' + apiToken;

      return $http.get(url);
    }
  };

  this.Admin = {
        all: function(params) {
          params.api_token = apiToken;
          var url = '/api/admin/list?' + $.param(params);

          return $http.get(url);
        },
        get: function(id){
          var url = '/api/admin/detail/' + id + '?api_token=' + apiToken;

          return $http.get(url);
        },
        create: function(params) {
          params.api_token = apiToken;

          var url = '/api/admin/create';
          return $http({
            method: 'POST',
            url:url,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(params)
          });
        },
        delete: function(id){
          var url = '/api/admin/' + id + '?api_token=' + apiToken;

          return $http.delete(url);
        }
      };

  this.User = {
    all: function() {
      var url = '/api/user/list?api_token=' + apiToken;

      return $http.get(url);
    },
    get: function(user_id) {
      var url = '/api/admin/profile/' + user_id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    add: function(post) {
      var params = post;
      var url = '/api/admin/profile?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    profile: function() {
      var url = '/api/profile?api_token=' + apiToken;

      return $http.get(url);
    },
    updateProfile: function(post) {
      var params = post;
      var url = '/api/profile?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    changePassword: function(post) {
      var params = post;
      var url = '/api/profile/change-password?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    forgotPassword: function(post) {
      var params = post;
      var url = '/api/profile/forgot-password?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    getDetail: function(user_id) {
      var url = '/api/admin/detail/' + user_id + '?api_token=' + apiToken;

      return $http.get(url);
    },
  };

  this.Project = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/project/list?' + $.param(params);

      return $http.get(url);
    },
    createPricePlan: function(id){
      var url = '/api/project/' + id + '/create-price-plan?api_token=' + apiToken;

      return $http.get(url);
    },
    changeWorker: function(params) {
      params.api_token = apiToken;
      var url = '/api/project/change-worker';

      return $http({
          method: 'POST',
          url: url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    setHpp: function(id, quotation_id) {
      var url = '/api/project/' + id + '/set-hpp-1/' + quotation_id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    unsetHpp: function(id) {
      var url = '/api/project/' + id + '/unset-hpp-1?api_token=' + apiToken;

      return $http.get(url);
    },
    unsetPricePlan: function(id) {
      var url = '/api/project/' + id + '/unset-hpp-2?api_token=' + apiToken;

      return $http.get(url);
    },
    changeStatus: function(params) {
      params.api_token = apiToken;
      var url = '/api/project/change-status';

      return $http({
          method: 'POST',
          url: url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    get: function(id) {
      var url = '/api/project/detail/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;
      var url = '/api/project/create';

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    delete: function(id){
      var url = '/api/project/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    itemListAvailable: function(id) {
      var url = '/api/project/' + id + '/price-plan/item/list/available?api_token=' + apiToken;

      return $http.get(url);
    },
    stock: function(params) {
      params.api_token = apiToken;
      var url = '/api/project/' + params.project_id + '/stock?' + $.param(params);

      return $http.get(url);
    },
    progress: function(params) {
      params.api_token = apiToken;
      var url = '/api/project/' + params.project_id + '/mandor-vendor-progress?' + $.param(params);

      return $http.get(url);
    },
    getPriceSummary: function(id){
      var url = '/api/project/' + id + '/price-summary?api_token=' + apiToken;

      return $http.get(url);
    },

    Checklist: {
      all: function(params) {
        params.api_token = apiToken;
        var url = '/api/project/checklist/list?' + $.param(params);

        return $http.get(url);
      },
      detail: function(id) {
        var url = '/api/project/' + id + '/checklist/detail?api_token=' + apiToken;

        return $http.get(url);
      },
      create: function(params) {
        params.api_token = apiToken;

        console.log('api', params);

        // params.api_token = apiToken;
        var url = '/api/project/checklist/create';
  
        return $http({
            method: 'POST',
            url:url,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(params)
        });
      },
      delete: function(id){
        var url = '/api/project/checklist/' + id + '?api_token=' + apiToken;
  
        return $http.delete(url);
      },

      Tick: {
        all: function(id) {
        var url = '/api/project/' + id + '/checklist/tick?api_token=' + apiToken;

        return $http.get(url);
        },
        create: function(params) {
          params.api_token = apiToken;

          console.log('api', params);

          // params.api_token = apiToken;
          var url = '/api/project/' + params.project_id + '/checklist/tick/create';
          // var url = '/api/project/checklist/create';
    
          return $http({
              method: 'POST',
              url:url,
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              data: $.param(params)
          });
        },

        detail: function(projectId,userId) {
          var url = '/api/project/' + projectId + '/checklist/tick/detail/' + userId + '?api_token=' + apiToken;
  
          return $http.get(url);
        },
    },
    }
  };

  this.Worker = {
    all: function(id) {
      var url = '/api/project/' + id + '/worker?api_token=' + apiToken;

      return $http.get(url);
    }
  };

  this.Category = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/category/list?' + $.param(params);

      return $http.get(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;

      var url = '/api/category/create';

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    delete: function(id){
      var url = '/api/category/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    }
  };

  this.Bank = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/bank/list?' + $.param(params);

      return $http.get(url);
    },
    create: function(post) {
      var params = post;
      params.api_token = apiToken;

      var url = '/api/bank/create';

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    delete: function(id){
      var url = '/api/bank/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    }
  };

  this.Config = {
    all: function() {
      var url = '/api/config?api_token=' + apiToken;

      return $http.get(url);
    },
    get: function(key) {
      var url = '/api/config/' + key + '?api_token=' + apiToken;

      return $http.get(url);
    },
    add: function(post) {
      var params = post;
      var url = '/api/config?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    }
  };


   this.InvoiceDelivery = {
    all: function(customer_id) {
      if(!customer_id)
        var url = '/api/invoice-delivery?api_token=' + apiToken;
      else
        var url = '/api/invoice-delivery?customer_id=' + customer_id + '&api_token=' + apiToken;

      return $http.get(url);
    },
    add: function(post) {
      var params = post;
      var url = '/api/invoice-delivery?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    get: function(id) {
      var url = '/api/invoice-delivery/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    unpayment: function(post) {
      var params = post;
      var url = '/api/invoice-delivery/unpayment?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    delete: function(id) {
      var url = '/api/invoice-delivery/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    }
  };

  this.InvoiceDeliveryRetur = {
    add: function(post) {
      var params = post;
      var url = '/api/invoice-delivery/retur?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    }
  };

  this.Invoice = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/invoice/list?' + $.param(params);

      return $http.get(url);
    },
    add: function(params) {
      var newParams = {
        api_token: apiToken,
        data: JSON.stringify(params)
      }
      // params.api_token = apiToken;
      var url = '/api/invoice/create';

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(newParams)
      });
    },
    get: function(id) {
      var url = '/api/invoice/detail/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/invoice/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    unpayment: function(post) {
      var params = post;
      var url = '/api/invoice/unpayment?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    detailList: function(post) {
      var params = post;
      var url = '/api/invoice/detail/list?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    updateItemAttribute: function(post) {
      var params = post;
      var url = '/api/invoice/update-item-attribute?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },

    search: function(params) {
      params.api_token = apiToken;
      var url = '/api/invoice/search?' + $.param(params);

      return $http.get(url);
    },
    download: function(id){
      var url = '/api/invoice/download/' + id + '?api_token=' + apiToken;

      return $http.get(url);
      },
    setStatus: function(id, status){
        var url = '/api/invoice/' + id + '/set-status/' + status +'?api_token=' + apiToken;

        return $http.get(url);
        },

    Payment: {
      all: function(params) {
        params.api_token = apiToken;
        var url = '/api/invoice/payment/list?' + $.param(params);
  
        return $http.get(url);
      },
      add: function(params) {
        var newParams = {
          api_token: apiToken,
          data: JSON.stringify(params)
        }
        // params.api_token = apiToken;
        var url = '/api/invoice/payment/create';
  
        return $http({
            method: 'POST',
            url:url,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param(newParams)
        });
      },
      get: function(id) {
        var url = '/api/invoice/payment/detail/' + id + '?api_token=' + apiToken;
  
        return $http.get(url);
      },
      delete: function(id) {
        var url = '/api/invoice/payment/' + id + '?api_token=' + apiToken;
  
        return $http.delete(url);
      }
    }
  };

  this.Receipt = {
        all: function(params) {
            params.api_token = apiToken;
            var url = '/api/receipt/list?' + $.param(params);

            return $http.get(url);
        },
        get: function(id) {
            var url = '/api/receipt/detail/' + id + '?api_token=' + apiToken;

            return $http.get(url);
        },
        create: function(params) {
            var newParams = {
                api_token: apiToken,
                data: JSON.stringify(params)
            }
            // params.api_token = apiToken;
            var url = '/api/receipt/create';

            return $http({
              method: 'POST',
              url:url,
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              data: $.param(newParams)
            });
        },
        delete: function(id) {
            var url = '/api/receipt/' + id + '?api_token=' + apiToken;

            return $http.delete(url);
        },
        download: function(id){
          var url = '/api/receipt/download/' + id + '?api_token=' + apiToken;

          return $http.get(url);
        },
        setStatus: function(id, status){
          var url = '/api/receipt/' + id + '/set-status/' + status +'?api_token=' + apiToken;

          return $http.get(url);
          },
    };

  this.Jobs = {
    all: function(params) {
      params.api_token = apiToken;
      var url = '/api/jobs/list?' + $.param(params);

      return $http.get(url);
    },
    create: function(params) {
      var newParams = {
          api_token: apiToken,
          data: JSON.stringify(params)
      }
      // params.api_token = apiToken;
      var url = '/api/jobs/create';

      return $http({
        method: 'POST',
        url:url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(newParams)
      });
    },
    get: function(id) {
        var url = '/api/jobs/detail/' + id + '?api_token=' + apiToken;

        return $http.get(url);
    },
    delete: function(id){
      var url = '/api/jobs/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    }
  };

  this.Expense = {
        all: function(params) {
            params.api_token = apiToken;
            var url = '/api/expense/list?' + $.param(params);

            return $http.get(url);
        },
        create: function(params) {
            var newParams = {
                api_token: apiToken,
                data: JSON.stringify(params)
            }
            // params.api_token = apiToken;
            var url = '/api/expense/create';

            return $http({
              method: 'POST',
              url:url,
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              data: $.param(newParams)
            });
        },
        get: function(id) {
            var url = '/api/expense/detail/' + id + '?api_token=' + apiToken;

            return $http.get(url);
        },
        delete: function(id){
        var url = '/api/expense/' + id + '?api_token=' + apiToken;

        return $http.delete(url);
        },
        setStatus: function(id, status){
          var url = '/api/expense/' + id + '/set-status/' + status +'?api_token=' + apiToken;

          return $http.get(url);
        }
  };

  this.InvoiceRetur = {
    add: function(post) {
      var params = post;
      var url = '/api/invoice/retur?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    }
  };

  this.InvoiceMandorVendor = {
    create: function(params) {
      // Because max_input_vars, so convert to json to make input_vars = 2 only
      var newParams = {
          api_token: apiToken,
          data: JSON.stringify(params)
      }
      // params.api_token = apiToken;
      var url = '/api/invoice/mandor-vendor/create';

      return $http({
        method: 'POST',
        url:url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(newParams)
      });
    },
    getOpname: function(id) {
      var url = '/api/invoice/mandor-vendor/opname/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    }
  };

  this.Order = {
    all: function(customer) {
      if(!customer)
        var url = '/api/orders?api_token=' + apiToken;
      else
        var url = '/api/orders?customer_id=' + customer + '&api_token=' + apiToken;

      return $http.get(url);
    },
    add: function(post) {
      var params = post;
      var url = '/api/order?api_token=' + apiToken;

      return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(params)
      });
    },
    get: function(id) {
      var url = '/api/order/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    delete: function(id) {
      var url = '/api/order/' + id + '?api_token=' + apiToken;

      return $http.delete(url);
    },
    archive: function(id) {
      var url = '/api/order/archive/' + id + '?api_token=' + apiToken;

      return $http.get(url);
    },
    search: function(params) {
      params.api_token = apiToken;
      var url = '/api/orders/search?' + $.param(params);

      return $http.get(url);
    }
  };

    this.Mendor = {
        all: function(params) {
            params.api_token = apiToken;
            var url = '/api/admin/list?roles=mandor&' + $.param(params);

            return $http.get(url);
        },
        get: function(id){
            var url = '/api/admin/detail/' + id + '?api_token=' + apiToken;

            return $http.get(url);
        },
        create: function(params) {
            params.api_token = apiToken;
            var url = '/api/admin/create';

            return $http({
                method: 'POST',
                url:url,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(params)
            });
        },
        delete: function(id){
            var url = '/api/admin/' + id + '?api_token=' + apiToken;

            return $http.delete(url);
        }
    };

    this.Transfer = {
      all: function(params) {
        params.api_token = apiToken;
        var url = '/api/transfer/list?' + $.param(params);

        return $http.get(url);
      },
      get: function(id){
        var url = '/api/transfer/detail/' + id + '?api_token=' + apiToken;

        return $http.get(url);
      },
      delete: function(id){
        var url = '/api/transfer/' + id + '?api_token=' + apiToken;

        return $http.delete(url);
      },
      create: function(params) {
        // Because max_input_vars, so convert to json to make input_vars = 2 only
        var newParams = {
            api_token: apiToken,
            data: JSON.stringify(params)
        }
        // params.api_token = apiToken;
        var url = '/api/transfer/create';

        return $http({
          method: 'POST',
          url:url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: $.param(newParams)
        });
      },
      setStatus: function(id, status){
        var url = '/api/transfer/' + id + '/set-status/' + status +'?api_token=' + apiToken;

        return $http.get(url);
      }
    };

    this.StockOpname = {
      all: function(params) {
        params.api_token = apiToken;
        var url = '/api/stock-opname/list?' + $.param(params);

        return $http.get(url);
      },
      get: function(id){
        var url = '/api/stock-opname/detail/' + id + '?api_token=' + apiToken;

        return $http.get(url);
      },
      delete: function(id){
        var url = '/api/stock-opname/' + id + '?api_token=' + apiToken;

        return $http.delete(url);
      },
      stocks: function(params){
        params.api_token = apiToken;
        var url = '/api/project/' + params.project_id + '/stock?' + $.param(params);

        return $http.get(url);
      },
      create: function(params) {
        console.log('create', params);

        var formData = new FormData();

        formData.append('api_token', apiToken);
        formData.append('data', JSON.stringify(params.data));
        for(var i=0; i<params.images.length; i++) {
            formData.append('images[]', params.images[i]);
        }

        var url = '/api/stock-opname/create';

        return $http({
            method: 'POST',
            url:url,
            headers: {
                'Content-Type': undefined,
            },
            data: formData
        });
      },
      setStatus: function(id, status){
        var url = '/api/stock-opname/' + id + '/set-status/' + status +'?api_token=' + apiToken;

        return $http.get(url);
      }
    };

    this.StockFlow = {
      all: function(params) {
        params.api_token = apiToken;
        var url = '/api/stock-flow/list?' + $.param(params);

        return $http.get(url);
      },
      category: function() {
        var url = '/api/stock-flow/get-category?api_token=' + apiToken;

        return $http.get(url);
      }
    };

    this.JobsOpname = {
      all: function(params) {
        params.api_token = apiToken;
        var url = '/api/job-opname/list?' + $.param(params);

        return $http.get(url);
      },
      create: function(params) {
        console.log('create', params);

        var formData = new FormData();

        formData.append('api_token', apiToken);
        formData.append('data', JSON.stringify(params.data));
        for(var i=0; i<params.images.length; i++) {
            formData.append('images[]', params.images[i]);
        }

        var url = '/api/job-opname/create';

        return $http({
            method: 'POST',
            url:url,
            headers: {
                'Content-Type': undefined,
            },
            data: formData
        });
      },
      get: function(id) {
          var url = '/api/job-opname/detail/' + id + '?api_token=' + apiToken;

          return $http.get(url);
      },
      delete: function(id){
        var url = '/api/job-opname/' + id + '?api_token=' + apiToken;

        return $http.delete(url);
      },
      setStatus: function(id, status){
        var url = '/api/job-opname/' + id + '/set-status/' + status +'?api_token=' + apiToken;

        return $http.get(url);
      }
    };

    this.Setting = {
      get: function() {
        var url = '/api/setting?api_token=' + apiToken;

        return $http.get(url);
      },
      create: function(params) {
        var newParams = {
            api_token: apiToken,
            data: JSON.stringify(params)
        }
        // params.api_token = apiToken;
        var url = '/api/setting';

        return $http({
        method: 'POST',
        url:url,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param(newParams)
        });
      },
    }

    this.Demo = {
        upload: function(params) {
            console.log('Demo upload');
            console.log(params);

            var formData = new FormData();

            formData.append('api_token', apiToken);
            for(var x in params.files) {
                formData.append('files[]', params.files[x]);
            }

            return $http({
                method: 'POST',
                url: '/api/demo/upload',
                headers: {
                    'Content-Type': undefined,
                },
                data: formData
            });
        }
    };

};

})();
